<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Contact;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        };
        $arrayimage = array("https://media1.tenor.com/images/49dc9a4311a5744e73ee6d5ebc3303b9/tenor.gif", 
                            "https://thumbs.gfycat.com/AdolescentMiserlyIguanodon-size_restricted.gif",
                            "https://i.makeagif.com/media/11-01-2015/n8Fygk.gif",
                            "https://media.giphy.com/media/hsUUH6Xj7IPrW/giphy.gif",
                            "https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg",
                            "https://media1.tenor.com/images/95fa4624e494393b165de6058fa75b7e/tenor.gif",
                            "http://78.media.tumblr.com/80ac8e6576bdaac670e2ba3e40715803/tumblr_ozlv9kfZpN1w8vjouo1_400.gif",
                            "https://media.giphy.com/media/oaMmESip8zrMI/giphy.gif",
                            "http://cdn140.picsart.com/234557713026202.gif",
                            "https://thumbs.gfycat.com/TinyUnhealthyAustraliansilkyterrier-size_restricted.gif"
        );
        for ($i = 0; $i < 10; $i++) {
        DB::table('contacts')->insert([
            [ 'fname' => generateRandomString(), 'sname' => generateRandomString(), 'number' => rand(1, 11000000), "image" => $arrayimage[$i]],
        ]);
        }
    }
}
