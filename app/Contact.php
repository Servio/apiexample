<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'fname',
        'sname',
        'number',
        'image'
    ];
}
